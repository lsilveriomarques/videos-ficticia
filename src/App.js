import React, { Fragment, Component } from 'react';
import Header from './components/Header/Header'
import HomePage from './pages/HomePage';

class App extends Component{
  
  constructor(props) {
    super(props);
    this.state = {
      textToSearch: '',
    }
  }

  setTextSearch = search => {
    this.setState({ textToSearch: search });
  }

  render () {
    const { textToSearch } = this.state;
    return (
      <Fragment>
        <Header sendSearchToHome={this.setTextSearch}/>
        <HomePage searchText={textToSearch}/>
      </Fragment>
    );
  }
  
}

export default App;
