import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import VideosService from '../services/videos.services'; 
import CardVideo from '../components/Videos/CardVideo/CardVideo';
import Config from '../config';
import Button from '@material-ui/core/Button';
import './homePage.scss';
import DetailsVideo from '../components/Videos/DetailsVideo/DetailsVideo';
import LoadDialog from '../components/Commons/Loads/LoadDialog';


class HomePage extends Component {
   
    constructor(props) {
        super(props);
        this.state = {
            videos: [],
            videosSearch: [],
            maxResults: 12,
            videoSelected: null,
            load: false,
            search: ''
        }
      }
    
    
    componentDidMount() {
        const { maxResults } = this.state;
        this.getAllVideos(maxResults);
    }

    componentDidUpdate(nextProps) {
        const { searchText } = this.props;

          if (nextProps.searchText !== searchText) {
            this.searchVideo(searchText);
          }
        
      }

    getAllVideos = maxResults => {
        this.setState({ load: true });
        VideosService.get('/search', { params: {
            part : 'snippet,id',
            key: Config.KEY_APY,
            maxResults: maxResults,
            channelId: Config.CHANNEL_ID
        }}).then(res => {
            const videos = res.data.items;
            this.setState({ videos });
            this.setState({ videosSearch: videos });
            this.setState({ load: false});
        }).catch( error => {
            this.setState({ load: false});
          })
    };

    getVideoById = id => {
        this.setState({ load: true });
        VideosService.get('/videos', { params: {
            part : 'snippet,contentDetails,statistics',
            key: Config.KEY_APY,
            id 
        }}).then(res => {
            const videoSelected = res.data.items[0];
            this.setState({videoSelected});
            this.setState({ load: false});
        
        }).catch( error => {
            this.setState({ load: false});
        })
    };

    searchVideo = search => {
        const { videos } = this.state;
        this.setState({ load: true, search: search });
        const find = videos.filter( video => video.snippet.title.toLowerCase().indexOf(search.toLowerCase()) !== -1 || video.snippet.description.toLowerCase().indexOf(search.toLowerCase()) !== -1 );
        if (search.length > 0) {
            this.setState({ videosSearch: find });
        } else{
            this.setState({ videosSearch: videos });
        }    
        this.setState({ load: false});
    };

    getMoreVideo = () => {
        const { maxResults } = this.state;
        const max = maxResults + 12;
        this.setState({ maxResults: max, load: true });
        this.getAllVideos(max);
    };

    seeMoreVideo = (videoSelected) => {
        this.getVideoById(videoSelected.id.videoId);
    }

    render (){
        const { videosSearch, videoSelected, load, search } = this.state;
        return (
            <Container maxWidth="md">
                { search.length > 0 ? (
                    <h1>Resultado para: {search}</h1> 
                ) : (
                    <h1>Todos os videos do canal</h1>
                    )
                }    
                <Grid container spacing={1}>
                    {videosSearch.length > 0 ? 
                        (
                            videosSearch.map( video =>(
                                <Grid container item xs={12} sm={6} md={4}>
                                    <a onClick={ () => this.seeMoreVideo(video) }>
                                        <CardVideo title={video.snippet.title} img={video.snippet.thumbnails.medium} />
                                    </a>
                                </Grid>
                            ))
                       ): (
                            <LoadDialog  action={ load }/>
                        )
                    }
                </Grid>
                { search.length > 0 ? (
                     ''
                ) : (
                    <Grid container item={12} justify="center">
                     <Button variant="outlined" size="large" onClick={this.getMoreVideo} classes={{ root: 'btn-more', label: 'text-btn-more'}}>
                         carregar mais videos ...
                     </Button>
                 </Grid>
                )
                }
                <DetailsVideo video={videoSelected} />
            </Container>
        )
    }
}

export default HomePage;
