import React from 'react';
import Container from '@material-ui/core/Container';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import InputSearch from '../Search/InputSearch';
import './header.scss';

const Header = props => {
  const { sendSearchToHome } = props;
  const sendSearch = search => {
    sendSearchToHome(search);
  }
    return( 
    <AppBar position="static" className="bar-color">
      <Container maxWidth="lg">
        <Toolbar className="space-between toolbar-color">
            <span className="box-brand">
              <img src="logo.png" alt="Logo Fictícia vídeos"/>
              <strong className="titulo">Fictícia vídeos</strong>
            </span>
            <InputSearch sendSearchToHeader={sendSearch}/>
        </Toolbar>
      </Container>
    </AppBar> 
  )
}

export default Header;