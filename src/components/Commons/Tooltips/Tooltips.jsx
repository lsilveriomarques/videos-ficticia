import React from 'react';
import PropTypes from 'prop-types';
import './tooltips.scss';

const Tooltips = props => {
    const { text, position, open } = props;
    return( 
        <div className="tooltip">
            { React.Children.map(props.children, child => child) }
            <span className={[ 'tooltiptext', position, open? 'tooltiptext' : '' ].join(' ')}>{text}</span>
        </div>        
    );
}

Tooltips.propTypes = {
    position: PropTypes.string,
    text: PropTypes.string,
    open: PropTypes.bool
};
  
Tooltips.defaultProps = {
    position: 'top',
    text: '',
    open: false
};
  
export default Tooltips;