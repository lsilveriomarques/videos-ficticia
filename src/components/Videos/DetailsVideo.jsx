import React, { useEffect, Fragment } from 'react';
import Dialog from '@material-ui/core/Dialog';
import './detailsVideo.scss';



const DetailsVideo = props =>  {
  const { video } = props;  
  const [open, setOpen] = React.useState(false);
console.log(video,'video')
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(
    () => {
        if (video !== null){
            handleOpen();
        }else{
            handleClose();
        }
    },
    [video]
  );

  return (
    <Fragment>
        <Dialog open={open} onClose={handleClose} >
            {video !== null? (
                <div className="container-modal-video">
                    <div className="box-icon-close"></div>
                    <div className="box-video">
                        <iframe  type="text/html" width="100%" height="100%" src={`http://www.youtube.com/embed/${video.id}`} frameborder="0" />
                    </div>
                    <div className="box-info">
                        <div className="box-info-over">
                            <div className="title-video">
                                {video.snippet.title}
                            </div>
                            <div className="box-icon-action">
                                <img />
                                <img />    
                            </div>
                           
                        </div>
                        <div className="box-description">
                            {video.snippet.description}
                        </div>     
                    </div>
                </div>
            ): (
                ''
            )}
            
        </Dialog>
    </Fragment>
  );
}

export default DetailsVideo;
