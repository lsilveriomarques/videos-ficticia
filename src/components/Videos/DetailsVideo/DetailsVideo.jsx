import React, { useEffect, Fragment } from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import './detailsVideo.scss';
import LoadDialog from '../../Commons/Loads/LoadDialog';
import Tooltips from '../../Commons/Tooltips/Tooltips';


const DetailsVideo = props =>  {
  const { video } = props;  
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  useEffect(
    () => {
        if (video !== null){
            handleOpen();
        }else{
            handleClose();
        }
    },
    [video]
  );

  return (
    <Fragment>
        <Dialog open={open} onClose={handleClose} >
            {video !== null? (
                <div className="container-modal-video">
                    <div className="box-icon-close">
                        <a onClick={handleClose}>
                            <img src="close.png" alt="Logo Fictícia vídeos"/>
                        </a>
                    </div>
                    <div className="box-video">
                        <iframe  type="text/html" width="100%" height="100%" src={`http://www.youtube.com/embed/${video.id}`} frameBorder="0" />
                    </div>
                    <div className="box-info">
                        <div className="box-info-over">
                            <div className="title-video">
                                {video.snippet.title}
                            </div>
                            <div className="box-icon-action">
                                <Tooltips text={`${video.statistics.viewCount} views`}>
                                    <a>
                                        <img src="eye-blue.png" alt="Logo Fictícia vídeos"/>
                                    </a>
                                </Tooltips>
                                <Tooltips text={`${video.contentDetails.duration.replace('PT','').replace('M', ':').replace('S', 's').replace('H', ':')}`}>
                                    <a>
                                        <img src="clock.png" alt="Logo Fictícia vídeos"/>
                                    </a>
                                </Tooltips>
                            </div>
                           
                        </div>
                        <div className="box-description">
                            {video.snippet.description}
                        </div>     
                    </div>
                </div>
            ): (
                <LoadDialog  action={video === null}/>
            )}
        </Dialog>
    </Fragment>
  );
}

export default DetailsVideo;

DetailsVideo.propTypes = {
    video: PropTypes.object.isRequired,
};
  
