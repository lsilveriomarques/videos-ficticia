import React from 'react';
import PropTypes from 'prop-types';
import './cardVideo.scss';

const CardVideo = props => {
    const { title, img } = props;
    const cutString = string =>{
        return string.substring(0, 65)+ '...';
    } 
    return( 
    <div className="box-card-video">
        <div className="box-img">
            <img src={img.url} alt="Imagem do video"/>
        </div>
        <div className="box-info">
            <h5 className="title-video">{cutString(title)}</h5>
            <span className="box-view">
                <img src="eyesicon.png" alt="Logo Fictícia vídeos"/>
                <span className="box-count">768 views</span>
            </span>  
        </div>
    </div>          
    );
}

export default CardVideo;
CardVideo.propTypes = {
    title: PropTypes.string,
    img: PropTypes.string,
};
  
CardVideo.defaultProps = {
    title: '',
    img: ''
};
  