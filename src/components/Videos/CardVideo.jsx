import React from 'react';
import './cardVideo.scss';

const CardVideo = props => {
    const { title, view, img, time } = props;
    const cutString = string =>{
        return string.substring(0, 45)+ '...';
    } 
    return( 
    <div className="box-card-video">
        <div className="box-img">
            <img src={img.url} alt="Imagem do video"/>
        </div>
        <div className="box-info">
            <h5>{cutString(title)}</h5>
            <span className="box-view">
                <img src="eyesicon.png" alt="Logo Fictícia vídeos"/>
                <span className="box-count">768 views</span>
            </span>  
        </div>
    </div>          
    );
}

export default CardVideo;