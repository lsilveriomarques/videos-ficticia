import React from 'react';
import SearchIcon from '@material-ui/icons/Search';
import './inputSearch.scss';
const InputSearch = props => {
  
  const { sendSearchToHeader } = props;
  
  const handleSearchVideo = e => {
    sendSearchToHeader(e.target.value)
  }
  
  return (
    <div className="search-box">
      <div className="icon-box">
        <SearchIcon />
      </div>
      <div className="input-box">
        <input type="text" onChange={handleSearchVideo} placeholder="Pesquisar..." className="input"/>
      </div>
    </div>
  )
}
export default InputSearch;